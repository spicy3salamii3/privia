﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterController : MonoBehaviour {

    public float speed = 10.0F;
    public AudioSource audio5;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //locks cursor inside game and makes it go
    }

    // Update is called once per frame
    void Update() { 
    
        float translation = Input.GetAxis("Vertical") * speed; //translation = forward backwards
        float straffe = Input.GetAxis("Horizontal") * speed;
        GetComponent<AudioSource>().UnPause();

        translation *= Time.deltaTime; //keeps movements in time with the update and keeps it smooth
        straffe *= Time.deltaTime;

        transform.Translate(straffe, 0, translation); //pushes it on the x and z axis

        if (Input.GetKeyDown("escape"))
            Cursor.lockState = CursorLockMode.None; //if hit escape key, turns of locked cursor

        if (Input.GetKeyDown("q"))
            Cursor.lockState = CursorLockMode.Locked; //if hit escape key, turns of locked cursor

        if (Input.GetKeyDown("g"))
            speed = 16.0F;

        if (Input.GetKeyUp("g"))
            speed = 10.0F;

        if (Input.GetKeyDown("left shift"))
            speed = 4.0F;


        if (Input.GetKeyUp("left shift"))
            speed = 10.0F;

    }
}
