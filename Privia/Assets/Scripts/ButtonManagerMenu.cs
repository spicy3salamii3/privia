﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManagerMenu : MonoBehaviour
{
    public void NewGameBtn(string newGameLevel)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(newGameLevel);
        gameObject.SetActive(true);
    }

    public void QuitGameBtn()
    {
        Application.Quit();
    }

}
