﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameras : MonoBehaviour
{
    private Camera camera1;

    void Start()
    {
        camera1 = GetComponent<Camera>();
        camera1.enabled = false;
        
    }
 
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F5))
        {
            camera1.enabled = false;

        }

        if (Input.GetKeyUp(KeyCode.F6))
        {
            camera1.enabled = true;

        }

    }

}
