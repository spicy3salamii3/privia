﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainColour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        props.SetColor("_Color", Color.green);
        GetComponent<Renderer>().SetPropertyBlock(props);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
