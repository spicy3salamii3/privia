﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauedGameQuit : MonoBehaviour
{
    public void Exit(string newGameLevel)
    {
        SceneManager.LoadScene(newGameLevel);
        gameObject.SetActive(true);
    }

}
