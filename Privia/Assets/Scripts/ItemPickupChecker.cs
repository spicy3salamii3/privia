﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickupChecker : MonoBehaviour
{
    //public Transform crossHair;

    private bool crossHairisOnTerrain = false;
    public float TargetDistance;

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
           
        if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out hit))
        {
            TargetDistance = hit.distance;
        }

        if (TargetDistance < 3)
        {
            crossHairisOnTerrain = true;
        }

        if (TargetDistance > 3)
        {
            crossHairisOnTerrain = false;
        }

        if (crossHairisOnTerrain)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("added snow to inventory");
            }
        }

    }
}
