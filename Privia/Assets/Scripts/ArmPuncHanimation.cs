﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmPuncHanimation : MonoBehaviour
{
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            StartCoroutine(MyCoroutine());
            anim.Play("PunchArm");

        }
    }

    IEnumerator MyCoroutine()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Animator>().Rebind();
    }
}

